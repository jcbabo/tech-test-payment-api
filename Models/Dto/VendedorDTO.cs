

namespace teste_tecnico_api_pagamentos.Models.Dto
{
    public class VendedorDTO
    {
        public string? CPF { get; set; }

        public string? NomeSobrenome { get; set; }

        public string? Email { get; set; }

        public string? Telefone { get; set; }
    }
}